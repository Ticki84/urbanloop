from tkinter import*
from random import gauss, random
from math import ceil
from numpy import mean
from win32api import GetSystemMetrics


# ----------------------------------------------------------------------- #
# Programme réalisé par Florent "Tonton" Caspar, étudiant en 2ème année   #
# à la prépa des INP de Nancy                                             #
#                                                                         #
# © COPYRIGHT 2018 Florent Caspar                                         #
# ----------------------------------------------------------------------- #

# Les unités sont écrites de la manière suivante: en {unité à entrée} ({unité réelle})
# Paramètres globaux
metre = 40  # en px/m (px/m)
frameRate = 60  # en FPS (img/s)
longueurFenetre = GetSystemMetrics(0)  # en px (px)
hauteurFenetre = GetSystemMetrics(1)  # en px (px)
hauteurParams = 400  # en px (px)
diametreTube = 2 * metre  # en m (px)
hauteurDevoiement = 0.5 * metre  # hauteur additionelle à celle du rayon du tube, en m (px)
tpsDevoiement = 2  # en s (s)
tpsRemiseSurRail = 2  # en s (s)
resolution = 0.1  # en s (s)
modeSaccade = False  # mode de déboguage (montre uniquement les actions principales)
frequenceSaccade = 0.05  # en s (s)
# Paramètres statistiques
modeStats = False
echantillon = 1000
tauxReussite = 99  # en %
# Paramètres des emplacements
longueurEmplacement = 3 * metre  # longueur d'un emplacement, en m (px)
nombreEmplacement = 8  # nombre d'emplacement(s)
# Paramètres de montée et descente des passagers
moyMontee = 10  # en s (s)
varMontee = 5  # en s (s)
moyDescente = 5  # en s (s)
varDescente = 2.5  # en s (s)
# Paramètres internes (ne pas les modifier)
capsules = []
emplacements = []
numCapsule = 0
tpsAvantApparition = 0  # (img)
nbCollision = 0
nbNDechargee = 0
nbCapsule = 0
nbBoucle = 0
todo = []
deltaT = 1  # (img)
tpsAvantVoyageur = 0  # (img)
voyageursGare = []
tpsGare = []
# Paramètres des capsules
longueurCapsule = 2.8 * metre  # en m (px)
diametreCapsule = 1.8 * metre  # en m (px)
vitesseCapsule = 10 * metre  # en m/s (px/s)
fluxApparition = 14  # (cap./min)
moyApparition = 60 / fluxApparition
varApparition = 5  # en s (s)
# Paramètres des voyageurs
probPleine = 50 / 100  # Probabilité que la capsule arrive avec un voyageur à l'intérieur (%)
fluxGare = 2  # (voy./min)
moyGare = 60 / fluxGare
varGare = 2.5  # en s (s)
# Paramètres calculés (ne pas les modifier)
centre = longueurFenetre - (1.5 + nombreEmplacement) * longueurEmplacement  # (px)
dxVitesse = vitesseCapsule / frameRate  # (px)
dyDevoiement = (diametreTube + hauteurDevoiement) / (tpsDevoiement * frameRate)  # (px)
dyRemiseSurRail = (diametreTube + hauteurDevoiement) / (tpsRemiseSurRail * frameRate)  # (px)


class Capsule:
    def __init__(self, t0, demandee=False):
        global numCapsule
        self.num = numCapsule  # Numéro de la capsule
        self.coords = [-longueurCapsule - t0 * dxVitesse, hauteurFenetre / 2 - diametreCapsule / 2]  # Coordonnées de la capsule (px)
        self.fig = canevas.create_rectangle(self.coords[0], self.coords[1], self.coords[0] + longueurCapsule, self.coords[1] + diametreCapsule, fill="coral") if not modeStats else None
        self.texte = canevas.create_text(self.coords[0] + longueurCapsule / 2, self.coords[1] + diametreCapsule / 2, justify=CENTER, text="N°0\n~\n~\n~") if not modeStats else None
        self.voyageur = canevas.create_image(self.coords[0] + longueurCapsule / 7, self.coords[1] + diametreCapsule / 8, image=voyageurA) if not modeStats and not demandee and random() <= probPleine else False
        self.depuisGare = False  # True si le voyageur vient de la gare
        self.affect = -1  # Emplacement affecté à la capsule, -1 si aucun emplacement
        self.surVoie = True  # True si encombre la voie
        self.doitMonter = False  # True si en train de monter
        self.peutDescendre = False  # True si en train de descendre
        self.arretEffectue = False  # True si la capsule s'est déjà arrêtée pour (dé)charger (dès la monter de la capsule)
        self.enCollision = False  # True si la capsule est actuellement en collision
        self.tempsRestant = 0  # Temps restant pour la montée, la descente ou le (dé)chargement de la capsule (img)
        self.dechargement = False  # True si la capsule est en train d'être déchargée
        self.raison = None  # None si la capsule n'est pas en attente, sinon contient la raison de l'attente
        numCapsule += 1


def affectation():
    # On dé-affecte les toutes les capsules qui n'ont pas déjà été arrêté
    for capsule in capsules:
        if not capsule.arretEffectue:
            capsule.affect = -1
    for capsule in capsules:
        # Si la capsule nécessite déchargement et qu'elle n'est pas déjà arrêtée
        if not capsule.arretEffectue:
            # On obtient le premier emplacement libre
            listeAffect = [item.affect for item in capsules if item.affect != -1 and item.affect != capsule.affect]
            for j in reversed(range(nombreEmplacement)):
                if j not in listeAffect:
                    # Si la capsule n'a pas dépassé l'emplacement
                    if capsule.coords[0] <= centre + j * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2:
                        # Et si l'emplacement disponible est plus éloigné que celui actuellement attribué
                        if j > capsule.affect:
                            # On vérifie qu'aucune capsule ne bloquera la capsule (i.e. en descente, en montée ou qui va monter avant l'emplacement disponible)
                            if len([item.affect for item in capsules if item.coords[0] > capsule.coords[0] and item.affect < j and (
                                (item.peutDescendre and item.coords[0] - (capsule.coords[0] + item.tempsRestant * dxVitesse) < (longueurEmplacement + longueurCapsule) / 2) or
                                (item.doitMonter and item.coords[0] - (capsule.coords[0] + item.tempsRestant * dxVitesse) < (longueurEmplacement + longueurCapsule) / 2) or
                                (item.surVoie and not item.arretEffectue and j > item.affect >= 0 and item.coords[0] - (capsule.coords[0] + tpsDevoiement * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2)
                            )]) == 0:
                                # On regarde s'il est inutile que notre capsule s'arrête
                                if not capsule.voyageur and j >= 0:
                                    # On vérifie d'abord que les capsules situées après et qui doivent dévoyer pourront le faire
                                    if len([item for item in capsules if capsule.coords[0] < item.coords[0] and item.affect >= 0 and (
                                        # Si l'item est déjà en train de dévoyer
                                        (item.doitMonter and item.coords[0] - (capsule.coords[0] + item.tempsRestant * dxVitesse) < (longueurEmplacement + longueurCapsule) / 2) or
                                        # Si l'item n'a pas encore dévoyer
                                        (item.surVoie and not item.arretEffectue and item.coords[0] - (capsule.coords[0] + tpsDevoiement * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2)
                                    )]) == 0:
                                        # On veut toujours au moins une capsule supplémentaire par rapport au nombre de personnes à attendre en gare
                                        # Les capsules avec voyageurs sont prioritaires dans l'affectation
                                        if len([item for item in capsules if not item.depuisGare and (item.affect >= 0 or item.voyageur)]) > len(tpsGare):
                                            capsule.affect = -1
                                            break
                                capsule.affect = j
                                break
                        else:
                            break
                    else:
                        break


def lancerCapsule():
    global tpsAvantApparition, todo
    # On vérifie qu'il n'y a pas de collision avec la précédente capsule avant d'en envoyer une nouvelle
    xmin = min([item.coords[0] for item in capsules]) if len(capsules) > 0 else 0
    tpsAvantApparition = max(int(ceil(gauss(moyApparition, varApparition) * frameRate) - xmin / dxVitesse), (longueurEmplacement - longueurCapsule - xmin) / dxVitesse)
    capsules.append(Capsule(tpsAvantApparition))
    todo.append([nbBoucle + tpsAvantApparition, "lancerCapsule"])


def apparaitreVoyageur():
    global todo, tpsGare, tpsAvantVoyageur
    tpsGare.append(0)
    for i in range(4):
        canevas.itemconfig(voyageursGare[i], text="+ {0} (moyenne: {1:.1f})".format(len(tpsGare) - 3 if len(tpsGare) > 3 else 0, mean(tpsGare) / frameRate if len(tpsGare) > 0 else 0) if i == 3 else ("{0:.1f}".format(tpsGare[i] / frameRate) if len(tpsGare) > i else "~"))
    tpsAvantVoyageur = int(ceil(gauss(moyGare, varGare) * frameRate))
    todo.append([nbBoucle + tpsAvantVoyageur, "apparaitreVoyageur"])


def updateDeltaT():
    global deltaT, modeSaccade
    modeSaccade = nModeSaccade.get()
    if modeStats or modeSaccade:
        listeT = []
        for capsule in capsules:
            if capsule.doitMonter:
                listeT.append(int(ceil((hauteurFenetre / 2 - diametreCapsule - diametreTube / 2 - hauteurDevoiement - capsule.coords[1]) / dyDevoiement)))
            elif capsule.peutDescendre:
                listeT.append(int(ceil((hauteurFenetre / 2 - diametreCapsule / 2 - capsule.coords[1]) / dyRemiseSurRail)))
            elif capsule.surVoie:
                if capsule.affect != -1:
                    listeT.append(int(ceil((centre - capsule.coords[0] + capsule.affect * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2) / dxVitesse)))
                if not capsule.enCollision:
                    listeCollision = [item for item in capsules if (item.doitMonter or item.peutDescendre or item.enCollision) and item.num != capsule.num and capsule.coords[0] < item.coords[0] + (longueurCapsule + longueurEmplacement) / 2]
                    if len(listeCollision) > 0:
                        for item in listeCollision:
                            listeT.append(int((item.coords[0] - capsule.coords[0] - (longueurCapsule + longueurEmplacement) / 2) / dxVitesse))
        listeT = [item for item in listeT if item > 0] + [item[0] - nbBoucle for item in todo if item[0] - nbBoucle > 0]
        deltaT = min(listeT) if len(listeT) > 0 else 1
    else:
        deltaT = 1


def updateInfo():
    global tpsAvantApparition, tpsAvantVoyageur
    tpsAvantApparition = max(tpsAvantApparition - deltaT, 0)
    lApparition.config(text="Prochaine apparition: {0:.1f}".format(tpsAvantApparition / frameRate))
    tpsAvantVoyageur = max(tpsAvantVoyageur - deltaT, 0)
    if not modeStats:
        for capsule in capsules:
            capsule.tempsRestant = max(capsule.tempsRestant - deltaT, 0)
            if capsule.raison is None:
                action = "~"
                temps = capsule.tempsRestant / frameRate if capsule.tempsRestant > 0 else "~"
                if capsule.surVoie:
                    if capsule.doitMonter or capsule.peutDescendre:
                        action = "Dévoiement" if capsule.doitMonter else "Remise sur voie"
                        temps = capsule.tempsRestant / frameRate
                else:
                    if capsule.voyageur:
                        action = "Montée voy." if capsule.depuisGare else "Descente voy."
                    else:
                        action = "Attente voy."
                        temps = tpsAvantVoyageur / frameRate
                if temps != "~":
                    canevas.itemconfig(capsule.texte, text="N°{0.num}\n{1}\n{2}\n{3:.1f}".format(capsule, "Affect. " + str(capsule.affect + 1) if capsule.affect >= 0 else "~", action, temps))
                else:
                    canevas.itemconfig(capsule.texte, text="N°{0.num}\n{1}\n{2}\n~".format(capsule, "Affect. " + str(capsule.affect + 1) if capsule.affect >= 0 else "~", action))
            else:
                canevas.itemconfig(capsule.texte, text="N°{0.num}\n{1}\n{2}".format(capsule, "Affect. " + str(capsule.affect + 1) if capsule.affect >= 0 else "~", capsule.raison))


def deplacerCapsules():
    global nbCollision, nbCapsule, nbNDechargee
    for capsule in reversed(capsules):
        coords = [capsule.coords[0], capsule.coords[1]]
        # Si la capsule est en dévoiement
        if capsule.doitMonter:
            coords[1] -= dyDevoiement * deltaT
            if coords[1] <= hauteurFenetre / 2 - diametreCapsule - diametreTube / 2 - hauteurDevoiement:
                coords[1] = hauteurFenetre / 2 - diametreCapsule - diametreTube / 2 - hauteurDevoiement
                canevas.itemconfig(emplacements[capsule.affect], fill='powderblue') if not modeStats else None
                capsule.doitMonter = False
                capsule.surVoie = False
                if capsule.voyageur:
                    capsule.tempsRestant = int(ceil(gauss(moyDescente, varDescente) * frameRate))
                    capsule.dechargement = True
                    todo.append([nbBoucle + capsule.tempsRestant, "charger", capsule])
                else:
                    charger(capsule)
        # Si la capsule est en remise sur voie
        elif capsule.peutDescendre:
            coords[1] += dyRemiseSurRail * deltaT
            if coords[1] >= hauteurFenetre / 2 - diametreCapsule / 2:
                coords[1] = hauteurFenetre / 2 - diametreCapsule / 2
                canevas.itemconfig(emplacements[capsule.affect], fill='powderblue') if not modeStats else None
                # capsule.tempsRestant = 0.0
                capsule.peutDescendre = False
                capsule.affect = -1
        # Si la capsule est sur la voie
        elif capsule.surVoie:
            coords[0] += dxVitesse * deltaT
            # Suppression de la capsule
            if capsule.coords[0] >= longueurFenetre:
                nbCapsule += 1
                lCapsule.config(text="Reçues: {0}".format(nbCapsule))
                lReussite.config(text="Réussite: {0:.2f}%".format((1 - (nbNDechargee + nbCollision) / nbCapsule) * 100) if nbCapsule > 0 else "Réussite: ~%")
                # Capsule non déchargée
                if not capsule.arretEffectue and capsule.voyageur and not capsule.depuisGare:
                    # print("La capsule " + str(capsule.num) + " ne s'est pas arrêté!")
                    nbNDechargee += 1
                    lNDechargee.config(text="Non déchargées: {0}".format(nbNDechargee))
                    lReussite.config(text="Réussite: {0:.2f}%".format((1 - (nbNDechargee + nbCollision) / nbCapsule) * 100) if nbCapsule > 0 else "Réussite: ~%")
                if not modeStats:
                    canevas.delete(capsule.fig)
                    canevas.delete(capsule.texte)
                    if capsule.voyageur:
                        canevas.delete(capsule.voyageur)
                del capsules[capsules.index(capsule)]
                continue
            # Capsule en collision
            listeCollision = [item for item in capsules if (item.doitMonter or item.peutDescendre or item.enCollision) and item.num != capsule.num and item.coords[0] - (longueurEmplacement + longueurCapsule) / 2 < coords[0] < item.coords[0] + (longueurEmplacement + longueurCapsule) / 2]
            if len(listeCollision) > 0:
                coords[0] = listeCollision[0].coords[0] - (longueurEmplacement + longueurCapsule) / 2 - 0.1 * dxVitesse
                if not capsule.enCollision:
                    # print("La capsule " + str(capsule.num) + " est entrée en collision avec la capsule " + str(listeCollision[0]))
                    nbCollision += 1
                    lCollision.config(text="Collisions: {0}".format(nbCollision))
                    lReussite.config(text="Réussite: {0:.2f}%".format((1 - (nbNDechargee + nbCollision) / nbCapsule) * 100) if nbCapsule > 0 else "Réussite: ~%")
                    capsule.enCollision = True
            # Si aucune collision, la capsule avance
            else:
                capsule.enCollision = False
                if not capsule.arretEffectue and capsule.affect != -1 and coords[0] >= centre + capsule.affect * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2:
                    coords[0] = centre + capsule.affect * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2
                    capsule.tempsRestant = int(ceil(tpsDevoiement * frameRate))
                    canevas.itemconfig(emplacements[capsule.affect], fill='sandybrown') if not modeStats else None
                    capsule.doitMonter = True
                    capsule.arretEffectue = True
        # Déplacement des capsules
        if not modeStats:
            canevas.move(capsule.fig, coords[0] - capsule.coords[0], coords[1] - capsule.coords[1])
            canevas.move(capsule.texte, coords[0] - capsule.coords[0], coords[1] - capsule.coords[1])
            if capsule.voyageur:
                canevas.move(capsule.voyageur, coords[0] - capsule.coords[0], coords[1] - capsule.coords[1])
        capsule.coords = coords


def charger(capsule):
    if capsule.voyageur and capsule.dechargement:
        canevas.delete(capsule.voyageur) if not modeStats else None
        capsule.voyageur = False
    capsule.dechargement = False
    capsule.tempsRestant = 0
    # On veut toujours une capsule en attente de voyageur de plus que le nombre de personnes attendant à la gare et on fait descendre celles de trop
    if len([item for item in capsules if not item.surVoie and item.affect > capsule.affect and not item.depuisGare and item.tempsRestant <= tpsAvantVoyageur]) > len(tpsGare):
        amorcerDescente(capsule)
    # On fait monter un voyageur en fonction de sa position (les plus à droite en priorité)
    elif len(tpsGare) > 0 and len([item for item in capsules if not item.surVoie and not item.depuisGare and item.affect > capsule.affect and item.tempsRestant == 0]) < len(tpsGare):
        del tpsGare[0]
        capsule.tempsRestant = int(ceil(gauss(moyMontee, varMontee) * frameRate))
        capsule.depuisGare = True
        capsule.voyageur = canevas.create_image(capsule.coords[0] + longueurCapsule / 7, capsule.coords[1] + diametreCapsule / 8, image=voyageurD) if not modeStats else True
        todo.append([nbBoucle + capsule.tempsRestant, "amorcerDescente", capsule])
    else:
        todo.append([nbBoucle + tpsAvantVoyageur, "charger", capsule])


def amorcerDescente(capsule):
    if not modeStats:
        canevas.itemconfig(emplacements[capsule.affect], fill='silver')
        canevas.itemconfig(capsule.fig, fill='limegreen')
    attente = False
    raison = "~"
    listeT = []
    # capsule: celle qui veut descendre
    # cap: une autre capsule
    for cap in capsules:
        if cap != capsule:
            # Si une cap doit se rendre après la capsule (cap sur voie hors montée/descente)
            if (cap.affect > capsule.affect or cap.affect == -1) and cap.surVoie and not cap.peutDescendre and not cap.doitMonter:
                # Si la cap n'a pas encore dépassée la capsule
                if cap.coords[0] - capsule.coords[0] < (longueurEmplacement + longueurCapsule) / 2:
                    # On regarde si la capsule a le temps de descendre avant que la cap n'arrive
                    if capsule.coords[0] - (cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2:
                        attente = True
                        raison = "Cap. " + str(cap.num) + " ne peut\npas passer"
                        if (cap.coords[0] > capsule.coords[0] - longueurCapsule):
                            raison = "Cap. " + str(cap.num) + " gêne\nla descente"
                        listeT.append(int(ceil(((longueurEmplacement + longueurCapsule) / 2 - cap.coords[0] + capsule.coords[0]) / dxVitesse)))
                        listeT.append(int(ceil(((longueurEmplacement + longueurCapsule) / 2 - capsule.coords[0] + cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) / dxVitesse)))
                        break
                # Si la cap a déjà dépassée la capsule
                else:
                    # On vérifie que la cap aura le temps de monter entièrement avant que la capsule arrive à son niveau
                    if (cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) - (capsule.coords[0] + tpsDevoiement * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2:
                        attente = True
                        raison = "Cap. " + str(cap.num) + " ne peut\npas dévoyer"
                        listeT.append(int(ceil(((longueurEmplacement + longueurCapsule) / 2 - cap.coords[0] + capsule.coords[0] + (tpsDevoiement - tpsRemiseSurRail) * vitesseCapsule) / dxVitesse)))
                        break
                # On regarde si la capsule ne ferait pas mieux d'attendre pour partir avec des capsules situées après celle-ci
                # TODO: revoir conditions
                # item: une autre capsule en gare que notre capsule pourrait attendre (et qui va également descendre)
                listeAffect = [item for item in capsules if not item.surVoie and capsule.affect < item.affect and item.depuisGare and (cap.affect == -1 or capsule.affect < cap.affect) and
                               # On vérifie que la cap a entièrement dépassée la capsule mais qu'elle [la cap] n'a pas encore dépassée l'item
                               (cap.coords[0] - capsule.coords[0] >= (longueurEmplacement + longueurCapsule) / 2 and cap.coords[0] - item.coords[0] < (longueurEmplacement + longueurCapsule) / 2) and
                               # On vérifie aussi que la capsule n'ait pas le temps de passer avant que item soit prêt à descendre (dans le cas où un voyageur est monté/en train de monter dans la capsule)
                               ((capsule.coords[0] + item.tempsRestant * dxVitesse) - (item.coords[0] + tpsRemiseSurRail * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2)]
                listeAffect += []
                if len(listeAffect) > 0:
                    attente = True
                    item = max(listeAffect, key=lambda t: t.tempsRestant)
                    raison = "Attente de\ncap. " + str(item.num)
                    listeT.append(item.tempsRestant)
                    listeT.append(((longueurEmplacement + longueurCapsule) / 2 - cap.coords[0] + item.coords[0]) / dxVitesse)
                    listeT.append((cap.coords[0] - capsule.coords[0] + (longueurEmplacement + longueurCapsule) / 2) / dxVitesse - item.tempsRestant)
                    break
            # Si une cap située avant la capsule a déjà entamée sa descente
            if cap.affect < capsule.affect and cap.peutDescendre:
                # On vérifie que la capsule aura le temps de descendre entièrement avant que la cap arrive à son niveau
                if (capsule.coords[0] + cap.tempsRestant * dxVitesse) - (cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) < (longueurEmplacement + longueurCapsule) / 2:
                    attente = True
                    raison = "Cap. " + str(cap.num) + " ne peut\nfinir sa remise"
                    listeT.append(int(ceil(((longueurEmplacement + longueurCapsule) / 2 - capsule.coords[0] + cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) / dxVitesse - cap.tempsRestant)))
                    break
            # Si une cap située après la capsule a déjà entamée sa montée
            elif cap.affect > capsule.affect and cap.doitMonter:
                # On vérifie que la cap aura le temps de monter entièrement avant que la capsule arrive à son niveau
                if (cap.coords[0] + tpsRemiseSurRail * vitesseCapsule) - (capsule.coords[0] + cap.tempsRestant * dxVitesse) < (longueurEmplacement + longueurCapsule) / 2:
                    attente = True
                    raison = "Cap. " + str(cap.num) + " ne peut\nfinir de dévoyer"
                    listeT.append(int(ceil(((longueurEmplacement + longueurCapsule) / 2 - cap.coords[0] + capsule.coords[0] - tpsRemiseSurRail * vitesseCapsule) / dxVitesse + cap.tempsRestant)))
                    break
    if not attente:
        capsule.tempsRestant = int(ceil(tpsRemiseSurRail * frameRate))
        if not modeStats:
            canevas.itemconfig(emplacements[capsule.affect], fill='sandybrown')
        capsule.raison = None
        capsule.surVoie = True
        capsule.peutDescendre = True
    else:
        capsule.raison = raison
        listeT = [item for item in listeT if item > 0]
        todo.append([nbBoucle + min(listeT), "amorcerDescente", capsule])


def anim():
    global tpsAvantApparition, todo, nbBoucle, tpsGare, tpsAvantVoyageur, capsules
    nbBoucle += deltaT
    updateInfo()
    sorted(capsules, key=lambda x: x.coords[0])
    tpsGare = [x + deltaT for x in tpsGare]
    for i in range(4):
        canevas.itemconfig(voyageursGare[i], text="+ {0} (moyenne: {1:.1f})".format(len(tpsGare) - 3 if len(tpsGare) > 3 else 0, mean(tpsGare) / frameRate if len(tpsGare) > 0 else 0) if i == 3 else ("{0:.1f}".format(tpsGare[i] / frameRate) if len(tpsGare) > i else "~"))
    deplacerCapsules()
    for item in reversed(todo):
        if nbBoucle >= item[0]:
            if item[1] == "amorcerDescente":
                amorcerDescente(item[2])
            elif item[1] == "lancerCapsule":
                lancerCapsule()
            elif item[1] == "apparaitreVoyageur":
                apparaitreVoyageur()
            elif item[1] == "charger":
                charger(item[2])
            del todo[todo.index(item)]
    affectation()
    updateDeltaT()
    fen.after(int(1000 / frameRate) if not modeSaccade else int(1000 * frequenceSaccade), anim)


def simuler():
    global capsules, numCapsule, tpsAvantApparition, nbCollision, nbNDechargee, nbCapsule, nbBoucle, todo, deltaT, tpsAvantVoyageur, voyageursGare, tpsGare
    capsules = []
    numCapsule = 0
    tpsAvantApparition = 0
    nbCollision = 0
    nbNDechargee = 0
    nbCapsule = 0
    nbBoucle = 0
    todo = []
    deltaT = 1
    tpsGare = []
    lCollision.config(text="Collisions: 0")
    lNDechargee.config(text="Non déchargées: 0")
    lReussite.config(text="Réussite: ~%")
    tpsAvantVoyageur = int(ceil(gauss(moyGare, varGare) * frameRate))
    todo.append([nbBoucle + tpsAvantVoyageur, "apparaitreVoyageur"])
    lancerCapsule()
    while (nbCapsule < echantillon):
        nbBoucle += deltaT
        updateInfo()
        sorted(capsules, key=lambda x: x.coords[0])
        tpsGare = [x + deltaT for x in tpsGare]
        deplacerCapsules()
        for item in reversed(todo):
            if nbBoucle >= item[0]:
                if item[1] == "amorcerDescente":
                    amorcerDescente(item[2])
                elif item[1] == "lancerCapsule":
                    lancerCapsule()
                elif item[1] == "apparaitreVoyageur":
                    apparaitreVoyageur()
                elif item[1] == "charger":
                    charger(item[2])
                del todo[todo.index(item)]
        affectation()
        updateDeltaT()
    for i in range(4):
        canevas.itemconfig(voyageursGare[i], text="+ {0} (moyenne: {1:.1f})".format(len(tpsGare) - 3 if len(tpsGare) > 3 else 0, mean(tpsGare) / frameRate if len(tpsGare) > 0 else 0) if i == 3 else ("{0:.1f}".format(tpsGare[i] / frameRate) if len(tpsGare) > i else "~"))
    # TODO: à fixer car le nombre de collisions est incohérent avec la simulation en saccade (qui elle est valide)


def calculerFlux():
    global nombreEmplacement, centre, fluxApparition, moyApparition, echantillon, deltaT, capsules, numCapsule, tpsAvantApparition, nbCollision, nbNDechargee, nbCapsule, nbBoucle, todo
    for j in range(3, 11):
        nombreEmplacement = j
        centre = longueurFenetre - (1.5 + nombreEmplacement) * longueurEmplacement
        a = (50 - sEchantillon.get()) / (nombreEmplacement ** (5 / 2) - 0.1)
        b = 50 - nombreEmplacement ** (5 / 2) * a
        fluxApparition = 1
        moyApparition = 60 / fluxApparition
        echantillon = 50
        nbEssai = 0
        delta = nombreEmplacement ** (5 / 2)
        while nbEssai < 2 or delta > 0.1:
            capsules = []
            numCapsule = 0
            tpsAvantApparition = 0
            nbCollision = 0
            nbNDechargee = 0
            nbCapsule = 0
            nbBoucle = 0
            todo = []
            deltaT = 1
            lCollision.config(text="Collisions: 0")
            lNDechargee.config(text="Non déchargées: 0")
            lReussite.config(text="Réussite: ~%")
            lancerCapsule()
            while (nbCapsule < echantillon):
                for capsule in capsules:
                    capsule.doitBouger = True
                    if capsule.tempsRestant > 0:
                        capsule.tempsRestant = max(capsule.tempsRestant - deltaT, 0)
                devoier()
                remise()
                deplacerCapsules()
                for item in reversed(todo):
                    if nbBoucle >= item[0]:
                        amorcerDescente(item[2]) if item[1] == "amorcerDescente" else lancerCapsule()
                        del todo[todo.index(item)]
                affectation()
                if tpsAvantApparition > 0:
                    tpsAvantApparition -= 1
                updateDeltaT()
                nbBoucle += deltaT
            if nbCollision + nbNDechargee <= echantillon * (100 - tauxReussite) / 100:
                if delta != 0.1:
                    nbEssai = 0
                    fluxApparition += delta
                    moyApparition = 60 / fluxApparition
                else:
                    nbEssai += 1
            else:
                fluxApparition -= delta
                delta /= 1.5
                if delta < 0.1:
                    delta = 0.1
                    nbEssai = 0
                moyApparition = 60 / fluxApparition
            echantillon = a * delta + b
        print("Flux maximal de {0:.0f} cap./h à {1:.1f}% sur un échantillon de {2:.0f} capsules pour {3} emplacements ({4} collision(s) et {5} non déchargée(s))".format(fluxApparition * 60, tauxReussite, echantillon, nombreEmplacement, nbCollision, nbNDechargee))
    echantillon = float(sEchantillon.get())
    nombreEmplacement = int(sEmplacement.get())
    fluxApparition = float(sFluxApparition.get())
    moyApparition = 60 / fluxApparition
    # TODO: réadapter le code du calcul de flux


def cProbPleine(value):
    global probPleine
    probPleine = float(value) / 100


def cFluxGare(value):
    global fluxGare, moyGare
    fluxGare = float(value)
    moyGare = 60 / fluxGare
    sMoyGare.set(moyGare)


def cMoyGare(value):
    global moyGare, fluxGare
    moyGare = float(value)
    fluxGare = 60 / moyGare
    sFluxGare.set(fluxGare)


def cVarGare(value):
    global varGare
    varGare = float(value)


def commanderVide():
    if not modeStats:
        pass
        # TODO


def envoyerVide():
    if not modeStats:
        pass
        # TODO


def cFluxApparition(value):
    global fluxApparition, moyApparition
    fluxApparition = float(value)
    moyApparition = 60 / fluxApparition
    sMoyApparition.set(moyApparition)


def cMoyApparition(value):
    global moyApparition, fluxApparition
    moyApparition = float(value)
    fluxApparition = 60 / moyApparition
    sFluxApparition.set(fluxApparition)


def cVarApparition(value):
    global varApparition
    varApparition = float(value)


def cMoyMontee(value):
    global moyMontee
    moyMontee = float(value)


def cVarMontee(value):
    global varMontee
    varMontee = float(value)


def cMoyDescente(value):
    global moyDescente
    moyDescente = float(value)


def cVarDescente(value):
    global varDescente
    varDescente = float(value)


def cFrequenceSaccade(value):
    global frequenceSaccade
    frequenceSaccade = float(value)


def cEchantillon(value):
    global echantillon
    echantillon = int(value)


def cTauxReussite(value):
    global tauxReussite
    tauxReussite = float(value)


def cEmplacement(value):
    global nombreEmplacement, centre
    if modeStats:
        nombreEmplacement = int(value)
        centre = longueurFenetre - (1.5 + nombreEmplacement) * longueurEmplacement
    else:
        sEmplacement.set(nombreEmplacement)


fen = Tk()
fen.geometry(str(longueurFenetre) + 'x' + str(hauteurFenetre))
fen.title("Urbanloop - Florent \"Tonton\" Caspar")
fen.state('zoomed')
canevas = Canvas(fen, width=longueurFenetre, height=hauteurFenetre - hauteurParams)
canevas.pack()
logo = PhotoImage(file="Urbanloop.gif")
canevas.create_image(400, 100, image=logo)
canevas.create_text(400, 180, justify=CENTER, text="Développé par Florent \"Tonton\" Caspar", fill="dimgrey", font=("Helvetica", 16))
voyageurA = PhotoImage(file="voyageurA.gif")
voyageurD = PhotoImage(file="voyageurD.gif")
params0 = LabelFrame(fen, text="Simulation", padx=5, pady=5)
params0.pack(side=LEFT, padx=longueurFenetre / 22)
nModeSaccade = IntVar()
if not modeStats:
    bModeSaccade = Checkbutton(params0, variable=nModeSaccade, text="Mode saccade")
    if modeSaccade:
        bModeSaccade.select()
    bModeSaccade.grid(row=0, column=0)
    sFrequenceSaccade = Scale(params0, command=cFrequenceSaccade, label="Fréquence de saccade", length=250, from_=0.01, to=1, resolution=0.01, orient=HORIZONTAL)
    sFrequenceSaccade.set(frequenceSaccade)
    sFrequenceSaccade.grid(row=1, column=0)
else:
    sEchantillon = Scale(params0, command=cEchantillon, label="Échantillons", length=250, from_=50, to=10000, resolution=50, orient=HORIZONTAL)
    sEchantillon.set(echantillon)
    sEchantillon.grid(row=0, column=0)
    sTauxReussite = Scale(params0, command=cTauxReussite, label="Taux de réussite", length=150, from_=95, to=99.9, resolution=resolution, orient=HORIZONTAL)
    sTauxReussite.set(tauxReussite)
    sTauxReussite.grid(row=1, column=0)
    sEmplacement = Scale(params0, command=cEmplacement, label="Nombre d'emplacements", length=150, from_=3, to=10, resolution=1, orient=HORIZONTAL)
    sEmplacement.set(nombreEmplacement)
    sEmplacement.grid(row=2, column=0)
    bSimuler = Button(params0, command=simuler, text="Simuler")
    bSimuler.grid(row=3, column=0, pady=5)
    bCalculerFlux = Button(params0, command=calculerFlux, text="Calcul du flux max.")
    bCalculerFlux.grid(row=4, column=0)
params1 = LabelFrame(fen, text="Voyageurs", padx=5, pady=5)
params1.pack(side=LEFT)
sProbPleine = Scale(params1, command=cProbPleine, label="Probabilité Capsule Pleine", length=150, from_=0, to=100, resolution=0.5, orient=HORIZONTAL)
sProbPleine.set(probPleine * 100)
sProbPleine.grid(row=0, column=0)
sFluxGare = Scale(params1, command=cFluxGare, label="Flux Moyen (pers./min)", length=150, from_=1, to=60, resolution=resolution, orient=HORIZONTAL)
sFluxGare.set(fluxGare)
sFluxGare.grid(row=1, column=0)
sMoyGare = Scale(params1, command=cMoyGare, label="Moyenne Apparition (gare)", length=150, from_=1, to=60, resolution=resolution, orient=HORIZONTAL)
sMoyGare.set(moyGare)
sMoyGare.grid(row=2, column=0)
sVarGare = Scale(params1, command=cVarGare, label="Variance Apparition (gare)", length=150, from_=0, to=10, resolution=resolution, orient=HORIZONTAL)
sVarGare.set(varGare)
sVarGare.grid(row=3, column=0)
if not modeStats:
    bCommanderVide = Button(params1, command=commanderVide, text="Commander une vide")
    bCommanderVide.grid(row=4, column=0, pady=5)
    bEnvoyerVide = Button(params1, command=envoyerVide, text="Envoyer une vide")
    bEnvoyerVide.grid(row=5, column=0)
params2 = LabelFrame(fen, text="Paramètres de la Gausienne des capsules", padx=5, pady=5)
params2.pack(side=LEFT, padx=longueurFenetre / 22)
sFluxApparition = Scale(params2, command=cFluxApparition, label="Flux Moyen (cap./min)", length=150, from_=1, to=60, resolution=resolution, orient=HORIZONTAL)
sFluxApparition.set(fluxApparition)
sFluxApparition.grid(row=0, column=0)
sMoyApparition = Scale(params2, command=cMoyApparition, label="Moyenne Apparition", length=150, from_=1, to=60, resolution=resolution, orient=HORIZONTAL)
sMoyApparition.set(moyApparition)
sMoyApparition.grid(row=1, column=0)
sVarApparition = Scale(params2, command=cVarApparition, label="Variance Apparition", length=150, from_=0, to=10, resolution=resolution, orient=HORIZONTAL)
sVarApparition.set(varApparition)
sVarApparition.grid(row=2, column=0)
params3 = LabelFrame(fen, text="Paramètres des Gaussiennes de montée et descente", padx=5, pady=5)
params3.pack(side=LEFT)
sMoyMontee = Scale(params3, command=cMoyMontee, label="Moyenne Montée", length=150, from_=0, to=20, resolution=resolution, orient=HORIZONTAL)
sMoyMontee.set(moyMontee)
sMoyMontee.grid(row=0, column=0)
sVarMontee = Scale(params3, command=cVarMontee, label="Variance Montée", length=150, from_=0, to=10, resolution=resolution, orient=HORIZONTAL)
sVarMontee.set(varMontee)
sVarMontee.grid(row=1, column=0)
sMoyDescente = Scale(params3, command=cMoyDescente, label="Moyenne Descente", length=150, from_=0, to=20, resolution=resolution, orient=HORIZONTAL)
sMoyDescente.set(moyDescente)
sMoyDescente.grid(row=0, column=1)
sVarDescente = Scale(params3, command=cVarDescente, label="Variance Descente", length=150, from_=0, to=10, resolution=resolution, orient=HORIZONTAL)
sVarDescente.set(varDescente)
sVarDescente.grid(row=1, column=1)
params4 = LabelFrame(fen, text="Informations", padx=5, pady=5)
params4.pack(side=LEFT, padx=longueurFenetre / 22)
lApparition = Label(params4, text="Prochaine apparition: 0.0")
lApparition.grid(row=0, column=0)
lCapsule = Label(params4, text="Reçues: 0")
lCapsule.grid(row=1, column=0)
lCollision = Label(params4, text="Collisions: 0")
lCollision.grid(row=2, column=0)
lNDechargee = Label(params4, text="Non déchargées: 0")
lNDechargee.grid(row=3, column=0)
lReussite = Label(params4, text="Réussite: ~%")
lReussite.grid(row=4, column=0)

for i in range(4):
    canevas.create_image(longueurFenetre / 2, 60 + 40 * i, image=voyageurD)
    voyageursGare.append(canevas.create_text(longueurFenetre / 2 + 40, 60 + 40 * i, anchor=W, text="+ {0} (moyenne: {1:.1f})".format(len(tpsGare) - 3 if len(tpsGare) > 3 else 0, mean(tpsGare) / frameRate if len(tpsGare) > 0 else 0) if i == 3 else ("{0:.1f}".format(tpsGare[i] / frameRate) if len(tpsGare) > i else "~")))
canevas.create_rectangle(0, hauteurFenetre / 2 - diametreTube / 2, longueurFenetre, hauteurFenetre / 2 + diametreTube / 2, fill="lavender")
for i in range(nombreEmplacement):
    emplacements.append(canevas.create_rectangle(centre + i * longueurEmplacement, hauteurFenetre / 2 - diametreTube / 2, centre + (i + 1) * longueurEmplacement, hauteurFenetre / 2 + diametreTube / 2, fill="powderblue"))
if not modeStats:
    tpsAvantVoyageur = int(ceil(gauss(moyGare, varGare) * frameRate))
    todo.append([nbBoucle + tpsAvantVoyageur, "apparaitreVoyageur"])
    lancerCapsule()
    anim()


# Capsules tests
# coords = [centre + 4 * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2, hauteurFenetre / 2 - diametreCapsule - diametreTube / 2 - hauteurDevoiement]
# capsules.append(Capsule(0))
# canevas.move(capsules[0].fig, coords[0] - capsules[0].coords[0], coords[1] - capsules[0].coords[1])
# canevas.move(capsules[0].texte, coords[0] - capsules[0].coords[0], coords[1] - capsules[0].coords[1])
# capsules[0].coords = coords
# capsules[0].affect = 4
# capsules[0].surVoie = False
# capsules[0].arretEffectue = True
# capsules[0].tempsRestant = 100
# todo.append([nbBoucle + capsules[0].tempsRestant, "charger", capsules[0]])
# coords = [centre + 5 * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2, hauteurFenetre / 2 - diametreCapsule - diametreTube / 2 - hauteurDevoiement]
# capsules.append(Capsule(0))
# canevas.move(capsules[1].fig, coords[0] - capsules[1].coords[0], coords[1] - capsules[1].coords[1])
# canevas.move(capsules[1].texte, coords[0] - capsules[1].coords[0], coords[1] - capsules[1].coords[1])
# capsules[1].coords = coords
# capsules[1].affect = 5
# capsules[1].surVoie = False
# capsules[1].arretEffectue = True
# capsules[1].tempsRestant = 380
# todo.append([nbBoucle + capsules[1].tempsRestant, "charger", capsules[1]])
# coords = [centre - 5 * longueurEmplacement + (longueurEmplacement - longueurCapsule) / 2, hauteurFenetre / 2 - diametreCapsule / 2]
# capsules.append(Capsule(0))
# canevas.move(capsules[2].fig, coords[0] - capsules[2].coords[0], coords[1] - capsules[2].coords[1])
# canevas.move(capsules[2].texte, coords[0] - capsules[2].coords[0], coords[1] - capsules[2].coords[1])
# capsules[2].coords = coords
# capsules[2].affect = 3

fen.mainloop()
